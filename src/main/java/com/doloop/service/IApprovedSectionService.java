package com.doloop.service;

import com.doloop.domain.RestResponse;

public interface IApprovedSectionService {
	
	RestResponse findAll(final String token);
	
	RestResponse getSectionsByDoc(final long Id ,final String token);

}
