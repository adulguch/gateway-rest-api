package com.doloop.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.doloop.domain.RestResponse;
import com.doloop.exception.RestException;

public interface ISourceDocService {
	
	RestResponse uploadSourceDoc(List<MultipartFile> files,final String token) throws IOException;
	
	RestResponse getSourceDocList(final String token);
	
	RestResponse extractBookmarks(final long docId, final String token)throws RestException;
	
	RestResponse getSourceSectionsBySourceDoc(final long docId, final String token)throws RestException;
	
	RestResponse extractBookmarkText(final long docId, Long sourceSectionId, final String token)throws RestException;

}
