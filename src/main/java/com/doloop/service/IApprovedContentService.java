package com.doloop.service;

import com.doloop.domain.RestResponse;

public interface IApprovedContentService {
	
	RestResponse findAll(final String token);
	
	RestResponse getContentBySection(final long Id, final String token);
	
	RestResponse getPropertyByContent(final long Id, final String token);
	
	RestResponse checkLatestVersion(final long Id, final long versionNo, final String token);
}
