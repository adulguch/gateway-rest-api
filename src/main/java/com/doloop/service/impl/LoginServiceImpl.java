package com.doloop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.doloop.model.Users;
import com.doloop.service.ILoginService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements ILoginService{

	@Autowired
	private Environment env;
	
	@Autowired
	private RestTemplate restTemplate;
	
	public String login(Users user) {
		log.debug("Login User -> " + user.getUsername());
		HttpEntity<Users> request = new HttpEntity<Users>(user);
		String url = env.getRequiredProperty("url.login");
		ResponseEntity<Users> res = restTemplate.exchange(url, HttpMethod.POST, request, Users.class);
		String authToken = res.getHeaders().get("Authorization").get(0);
		log.info("User -> " + user.getUsername() + " Token -> " + authToken);
		return authToken;
	}
}
