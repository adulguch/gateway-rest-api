package com.doloop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.doloop.domain.RestResponse;
import com.doloop.service.IApprovedContentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ApprovedContentServiceImpl implements IApprovedContentService{
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private Environment env;
	
	public RestResponse findAll(final String token) {
//		log.debug("Finding all ApprovedContent entries.");
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.approvedContent.all");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url, HttpMethod.GET, entity, RestResponse.class);
		final RestResponse response = res.getBody();
		return response;
	}

	public RestResponse getContentBySection(long Id, String token) {
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.approvedContentBySection.getContentBySection");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url+Id, HttpMethod.GET, entity, RestResponse.class);
        log.debug("URL-->  " + url+Id);
        final RestResponse response = res.getBody();
		return response;
	}

	public RestResponse getPropertyByContent(long Id, String token) {
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.propertyByContent.getPropertyByContent");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url+Id, HttpMethod.GET, entity, RestResponse.class);
        log.debug("URL-->  " + url+Id);
        final RestResponse response = res.getBody();
		return response;
		}

	public RestResponse checkLatestVersion(long Id, long versionNo, String token) {
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.latestVersion.checkLatestVersion");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url+Id+"/"+versionNo, HttpMethod.GET, entity, RestResponse.class);
        log.debug("URL-->  " + url+Id+"/"+versionNo);
        final RestResponse response = res.getBody();
		return response;
	}
}
