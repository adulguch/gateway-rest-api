package com.doloop.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.doloop.domain.RestResponse;
import com.doloop.service.IApprovedContentService;
import com.doloop.service.IApprovedDocService;
import com.doloop.service.IApprovedSectionService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ApprovedDocServiceImpl implements IApprovedDocService {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private IApprovedSectionService approvedSectionService;
	
	@Autowired
	private IApprovedContentService approvedContentService;
	
	
	
	public RestResponse findAll(final String token) {
//		log.debug("Finding all ApprovedDoc entries.");
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.approvedDoc.all");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url, HttpMethod.GET, entity, RestResponse.class);
        log.debug("URL-->  " + url);
        final RestResponse response = res.getBody();
		return response;
	}

	public RestResponse getById(long Id,String token) {
		log.debug("Finding ApprovedDoc :" + Id);
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.approvedDocById.getById");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url+Id, HttpMethod.GET, entity, RestResponse.class);
        log.debug("URL-->  " + url+Id);
        final RestResponse response = res.getBody();
		return response;
	}

	public RestResponse getAllDataById(long Id, String token) {
		log.debug("Finding ApprovedDoc All Data :" + Id);
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.approvedDocAllData.allData");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url+Id, HttpMethod.GET, entity, RestResponse.class);
        log.debug("URL-->  " + url+Id);
        final RestResponse response = res.getBody();
		return response;
	}

	


}
