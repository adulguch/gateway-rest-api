package com.doloop.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.doloop.domain.RestResponse;
import com.doloop.exception.RestException;
import com.doloop.service.ISourceDocService;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SourceDocServiceImpl implements ISourceDocService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private Environment env;

	public RestResponse uploadSourceDoc(List<MultipartFile> files, String token) throws IOException {
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		files.forEach(file -> {
			System.out.println(file.getOriginalFilename());
			map.add("file",  new FileSystemResource(convert(file)));
		});
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.set("Authorization", token);
        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        String url = env.getRequiredProperty("url.sourceDoc.upload");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url, HttpMethod.POST, requestEntity, RestResponse.class);
        log.debug("URL-->  " + url);
        final RestResponse response = res.getBody();
        files.forEach(file -> {
			File convFile = new File(file.getOriginalFilename());
			boolean bool = convFile.delete();
	        System.out.println("File "+file.getOriginalFilename()+" deleted after upload : "+bool);
		});
		return response;
	}
	
	public static File convert(MultipartFile file)
	  {    
	    File convFile = new File(file.getOriginalFilename());
	    try {
	        convFile.createNewFile();
	          FileOutputStream fos = new FileOutputStream(convFile); 
	            fos.write(file.getBytes());
	            fos.close(); 
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } 

	    return convFile;
	 }
	
	@Override
	public RestResponse extractBookmarks(long docId, String token) throws RestException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", token);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		final RestResponse resp = getSourceSectionsBySourceDoc(docId, token);
		String jsonString = new Gson().toJson(resp.getData());
		Gson googleJson = new Gson();
		@SuppressWarnings("rawtypes")
		ArrayList javaArrayListFromGSON = googleJson.fromJson(jsonString, ArrayList.class);

		if(javaArrayListFromGSON == null)
		{
			final RestResponse response = RestResponse.builder().success(false)
					.message(resp.getMessage()).build();
			return response;
		}
		
		if (javaArrayListFromGSON.size() > 0) {
			final RestResponse response = RestResponse.builder().success(false)
					.message("Bookmarks are already extracted for the source document").build();
			return response;
		} else {
			String url = env.getRequiredProperty("url.sourceDoc.bookmarkExtract");
			ResponseEntity<RestResponse> res = restTemplate.exchange(url + docId, HttpMethod.POST, entity,RestResponse.class);
			log.debug("Extract URL-->  " + url + docId);
			final RestResponse response = getSourceSectionsBySourceDoc(docId, token);
			return response;
		}
	}
	
	@Override
	public RestResponse getSourceSectionsBySourceDoc(long docId, String token) throws RestException {
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.sourceSection.sourceSectionsBySourceDoc");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url+docId, HttpMethod.GET, entity, RestResponse.class);
        log.debug("URL-->  " + url+docId);
        final RestResponse response = res.getBody();
		return response;
	}
	
	@Override
	public RestResponse extractBookmarkText(long docId, Long sourceSectionId, String token) throws RestException {
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.sourceSection.extractBookmarkText");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url+docId+"/"+sourceSectionId, HttpMethod.POST, entity, RestResponse.class);
        log.debug("URL-->  " + url+docId+"/"+sourceSectionId);
        
        String textUrl = env.getRequiredProperty("url.sourceSection.extractedBookmarkWithText");
        ResponseEntity<RestResponse> resp = restTemplate.exchange(textUrl+docId+"/"+sourceSectionId, HttpMethod.GET, entity, RestResponse.class);
        log.debug("Text URL-->  " + textUrl+docId+"/"+sourceSectionId);
        final RestResponse response = resp.getBody();
		return response;
	}

	@Override
	public RestResponse getSourceDocList(String token) {
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = env.getRequiredProperty("url.sourceDoc.getSourceDocList");
        ResponseEntity<RestResponse> res = restTemplate.exchange(url, HttpMethod.GET, entity, RestResponse.class);
        log.debug("URL-->  " + url);
        final RestResponse response = res.getBody();
		return response;
	}
}
