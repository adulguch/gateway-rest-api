package com.doloop.service;

import com.doloop.model.Users;

public interface ILoginService {
	String login(Users user);
}
