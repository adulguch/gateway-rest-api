package com.doloop.service;

import com.doloop.domain.RestResponse;

public interface IApprovedDocService {
	
	RestResponse findAll(final String token);
	
	RestResponse getById(final long Id, final String token);
	
	RestResponse getAllDataById(final long Id, final String token);

}
