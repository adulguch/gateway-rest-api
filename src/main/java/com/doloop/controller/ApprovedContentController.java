package com.doloop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.doloop.domain.RestResponse;
import com.doloop.exception.RestException;
import com.doloop.model.Users;
import com.doloop.service.IApprovedContentService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/api/approvedContent")
public class ApprovedContentController {

	@Autowired
	private IApprovedContentService approvedContentService;
	
	/* 
	 * url http://localhost:7773/content-similarity/api/approvedContent/
	 * get data with auth bearer token
	 */
	@GetMapping("/all")
	public RestResponse findAll(@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding all approvedContent entries.");
		return approvedContentService.findAll(token);
	}
	
	@GetMapping("/getContentBySection/{Id}")
	public RestResponse getContentBySection(@PathVariable("Id") final long Id ,@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding approvedContent By Section ID {}  : " + Id );
		return approvedContentService.getContentBySection(Id,token);
	}
	
	@GetMapping("/getPropertyByContent/{Id}")
	public RestResponse getPropertyByContent(@PathVariable("Id") final long Id ,@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding property By Content ID {}  : " + Id );
		return approvedContentService.getPropertyByContent(Id,token);
	}
	
	@GetMapping("/checkLatestVersion/{Id}/{versionNo}")
	public RestResponse checkLatestVersion(@PathVariable("Id") final long Id , @PathVariable("versionNo") final long versionNo, @RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding Latest Content version {}  : " + versionNo );
		return approvedContentService.checkLatestVersion(Id,versionNo,token);
	}
}
