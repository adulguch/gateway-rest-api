package com.doloop.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.doloop.domain.RestResponse;
import com.doloop.exception.RestException;
import com.doloop.service.ISourceDocService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/api/sourceDoc")
public class SourceDocController {

	@Autowired
	private ISourceDocService sourceDocService;
	
	
	@PostMapping("/upload")
	public RestResponse uploadSourceDoc(@RequestParam("file") List<MultipartFile> files, @RequestHeader("${jwt.header}") final String token) throws RestException, IOException {
		log.debug("Uploading Document");
		return sourceDocService.uploadSourceDoc(files, token);
	}
	
	@GetMapping("/getSourceDocList")
	public RestResponse getSourceDocList(@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding all Source Doc entries.");
		return sourceDocService.getSourceDocList(token);
	}
	
	@GetMapping("/extractBookmarks/{docId}")
	public RestResponse extractBookmarks(@PathVariable("docId") Long docId, @RequestHeader("${jwt.header}") final String token) throws RestException, IOException {
		log.debug("Extracting Bookmarks for the doc with Id: " +docId);
		return sourceDocService.extractBookmarks(docId, token);
	}
	
	@GetMapping("/getSourceSectionsBySourceDoc/{docId}")
	public RestResponse getSourceSectionsBySourceDoc(@PathVariable("docId") Long docId, @RequestHeader("${jwt.header}") final String token) throws RestException, IOException {
		log.debug("Getting Sections for the doc with Id: " +docId);
		return sourceDocService.getSourceSectionsBySourceDoc(docId, token);
	}
	
	@GetMapping("/extractBookmarkText/{docId}/{sourceSectionId}")
	public RestResponse extractBookmarkText(@PathVariable("docId") Long docId,@PathVariable("sourceSectionId") Long sourceSectionId, @RequestHeader("${jwt.header}") final String token) throws RestException, IOException {
		log.debug("Extracting text Bookmarks for with Id: " +sourceSectionId);
		return sourceDocService.extractBookmarkText(docId,sourceSectionId, token);
	}
}
