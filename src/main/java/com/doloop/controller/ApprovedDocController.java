package com.doloop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.doloop.domain.RestResponse;
import com.doloop.exception.RestException;
import com.doloop.service.IApprovedDocService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/approvedDoc")
@Slf4j
public class ApprovedDocController {
	
	@Autowired
	private IApprovedDocService approvedDocService;
	
	@GetMapping("/getAllDocList")
	public RestResponse findAll(@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding all approvedDoc entries.");
		return approvedDocService.findAll(token);
	}
	
	@GetMapping("/getDocDetails/{Id}")
	public RestResponse getById(@PathVariable("Id") final long Id ,@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding approvedDoc detials {}  : " + Id );
		return approvedDocService.getById(Id,token);
	}
	
	@GetMapping("/getAllDocDataById/{Id}")
	public  RestResponse getAllDataById(@PathVariable("Id") final long Id ,@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding approvedDoc detials with all data {}  : " + Id );
		return approvedDocService.getAllDataById(Id, token);
	}

}
