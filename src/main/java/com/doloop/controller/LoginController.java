package com.doloop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.doloop.domain.RestResponse;
import com.doloop.exception.RestException;
import com.doloop.model.Users;
import com.doloop.service.ILoginService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
public class LoginController {
	
	@Autowired
	ILoginService loginService;
	
	/*
	 * url: http://localhost:7773/content-similarity/api/login/
	 * post with json body as
	 * {
		    "username": "admin",
		    "password": "password"
		}
	 */
	@PostMapping("/login")
	public RestResponse login(@RequestBody Users user) throws RestException {
		String authToken = loginService.login(user);
		final RestResponse response = RestResponse.builder().data(authToken).success(true).build();
		return response;
	}
}
