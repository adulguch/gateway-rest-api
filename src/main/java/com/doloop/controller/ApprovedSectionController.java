package com.doloop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.doloop.domain.RestResponse;
import com.doloop.exception.RestException;
import com.doloop.service.IApprovedSectionService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/approvedSection")
@Slf4j
	
public class ApprovedSectionController {
	
	@Autowired
	private IApprovedSectionService approvedSectionService;
	
	@GetMapping("/getAllSectionList")
	public RestResponse findAll(@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding all approvedSection entries.");
		return approvedSectionService.findAll(token);
	}
	
	@GetMapping("/getAllSectionList/{Id}")
	public RestResponse getById(@PathVariable("Id") final long Id ,@RequestHeader("${jwt.header}") final String token) throws RestException {
		log.debug("Finding all approvedSection entries with Doc ID {}  : " + Id );
		return approvedSectionService.getSectionsByDoc(Id,token);
	}


}
