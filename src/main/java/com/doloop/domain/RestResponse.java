package com.doloop.domain;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class RestResponse {
	Object data;
	boolean success;
	String message;
	List<ValidationError> errors;

	@Builder
	public RestResponse(Object data, boolean success, String message, List<ValidationError> errors) {
		super();
		this.data = data;
		this.success = success;
		this.message = message;
		this.errors = errors;
	}
}
